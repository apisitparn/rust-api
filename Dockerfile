FROM rust:1.43.1-slim-stretch

USER root


ENV USER=kuy
ENV http_proxy http://p1102700374531:9999@172.16.1.8:3128
ENV https_proxy http://p1102700374531:9999@172.16.1.8:3128

COPY . /usr/src/myapp/
WORKDIR /usr/src/myapp/hero-api/

RUN apt-get update && apt-get install -y ca-certificates wget
RUN rustup install nightly
RUN rustup default nightly
RUN rustup update && cargo update
RUN cargo --version && rustc --version
# RUN cargo install --path .

CMD ["cargo","run"]

## 2 หัวข้อแรกทำแล้ว ทำต่อที่ Persisting our data via ORM (Diesel)
#https://medium.com/sean3z/building-a-restful-crud-api-with-rust-1867308352d8

#docker
#docker build -t my-rust-app .
#docker build --build-arg http_proxy --build-arg https_proxy -t my-rust-app .
#docker run -v //c/Users/apisi/Rust://usr/src/myapp -p 8000:8000 --name my-running-app my-rust-app

#docker rm -f $(docker ps -a -q)

####rust create folder
#cargo new hero-api --bin && cd hero-api
#cd hero-api

###rust 
#rustup install nightly
#rustup default nightly
#rustup update && cargo update
#cargo --version && rustc --version
#cargo run